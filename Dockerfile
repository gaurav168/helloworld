FROM golang:1.16-alpine

WORKDIR /app

# Download Go modules
COPY go.mod .
COPY go.sum .
RUN go mod download

COPY *.go ./

#BUILD
RUN go build  -o /helloworld

# Exposing on port 3000
EXPOSE 3000

CMD [ "/helloworld" ]
